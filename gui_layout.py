# You can easily modify element positions here
WIDGET_POSITIONS = {'Action':(55, 20),
                    'Operation':(325, 20),
                    'Config':(450, 20),
                    'Port':(600, 20),
                    'Data':(25, 50),
                    'Transfer':(15, 520),
                    'Archive':(630, 520),
                    'Raw':(750, 520),
                    'Copy':(780, 65),
                    'Send':(723, 430),
                    'Send Data':(25, 430)}

TEXT_POSITIONS = {'Action:':(5, 20),
                  'File:':(290, 20),
                  'Port:':(550, 20),
                  'Transfer To:':(150, 520)}
                 
CONFIG_WIDGET_POSITIONS = {'Download':(210, 20),
                           'Upload':(210, 60),
                           'Baud':(208, 100),
                           'Timeout':(210, 140),
                           'Ok':(90, 200),
                           'Cancel':(210, 200)}

CONFIG_TEXT_POSITIONS = {'Download Data Location:':(190, 20), 
                         'Upload Config Location:':(190, 60),
                         'Baud:':(190, 100),
                         'Timeout:':(190, 140)}