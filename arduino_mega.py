import serial
from serial.tools import list_ports
import time
from collections import namedtuple, OrderedDict
import sys

class ArduinoMega2560(object):
    VERSION = '1.0'
    AUTHOR = 'William Weatherholtz'
    
    # Our default COM port contains this string in its ID
    SERIAL_NAME_STRING = 'COM'#'Arduino'
    
    # Arduino input buffer size in bytes
    BUFFER_SIZE = 256
    # Seconds to wait after connection for Arduino program to start.
    CONNECT_DELAY = 10
    
    CODING_SCHEME = 'ascii'
    
    def __init__(self, port=None, baudrate=57600, timeout=2000):
        self.port = port
        self.timeout = timeout
        self.baudrate = baudrate
        self.is_opened = False
        
        if port is None:
            self.port = self.auto_port()

    @staticmethod
    def list_ports():
        ''' lists all com ports without having to instantiate class first'''
        comport = namedtuple('COM_Port', ['port', 'name', 'ID'])        
        return [comport(*info) for info in list_ports.comports()]#list(list_ports.comports())
    
    def open(self):
        self.__enter__()
    
    def close(self):
        if self.is_opened:
            self.serial.close()
        
    def __enter__(self): 
        print("Opening Port")
        self.serial = serial.Serial(self.port,
                                    baudrate=self.baudrate,
                                    timeout=self.timeout)
                                    
        # Sleep might be needed because arduinos aren't as fast as computers
        #time.sleep(2)
        self.is_opened = True
        wait_end = time.time() + self.CONNECT_DELAY;
        while time.time() < wait_end:
            read = self.serial.readline().decode(self.CODING_SCHEME)
            # FIX: If raw is checked, write to terminal.
            print(read,end=None)
        print("SETUP DONE")
        return self

    def __exit__(self, exctype, excvalue, exctb):
        self.close()

    def auto_port(self):
        '''Automatically selects port upon finding self.SERIAL_NAME_STRING'''
        for port, name, ID in list_ports.comports():
            if self.SERIAL_NAME_STRING in name:
                return port
        raise Exception('Arduino could not be automatically detected')

    def send(self, message):
        '''Formats, encodes, and sends message via serial.
           Also ensures buffer doesn't overflow'''
        if not self.is_opened:
            self.open()
        message = message.encode(self.CODING_SCHEME)
        
        # RX buffer needs to be cleared before every command
        self.serial.flushInput()
        
        # Wait until buffer has room.  Prevents overrun
        while self.serial.outWaiting() + len(message) > self.BUFFER_SIZE:
            pass

        self.serial.write(message)
        # If raw is checked, write to terminal.
        print("SEND:{}".format(message))

    def wait_for(self, expected_response):
        '''Waits for a serial response'''
        print("EXPECT:{}".format(expected_response))
        while True:
            try:
                read = self.serial.readline().decode(self.CODING_SCHEME)
                # FIX: If raw is checked, write to terminal.
                print(read,end=None)
                #if read[:len(expected_response)] == expected_response[:len(expected_response)]:
                if expected_response in read:
                    print("FOUND")
                    time.sleep(1)
                    break
            except:
                print("Unexpected error:", sys.exc_info()[0])
                continue

    def get_data_until(self, end_string):
        ''' Updates GUI Window and reads data'''
        data = []
        print("RECORDING UNTIL:{}".format(end_string))
        while True:
            if self.serial.inWaiting() > 0:
                try:
                    read = self.serial.readline().decode(self.CODING_SCHEME)
                    # FIX: If raw is checked, write to terminal.
                    print(read,end=None)
                    #if read == end_string:
                    if end_string in read:
                        print("FOUND:{}".format(end_string))
                        time.sleep(1)
                        break
                except:
                    continue
                data.append(read)
                
        #{FILE:<filename>.csv,RECEIVED:<size in bytes>}\r
        return '\n'.join(data)
        
    def upload_intro(self):
        ''' Runs through upload initialization sequence'''
        print("upload_intro")
        
    def truncate_log(self):
        print("truncate_log")
        self.console_mode()
        self.main_menu()
        self.file_menu()
        self.send('8')
        self.wait_for('LOG DELETED')
        
    def archive_intro(self):
        print("archive_intro")
        self.console_mode()
        self.main_menu()
        self.file_menu()
        self.send('1')
        self.wait_for('ARCHIVING FILES')
        
    def outro(self):
        ''' Runs through teardown sequence'''
        print("outro")
        #self.main_menu()
        time.sleep(1)
        self.send('~0')
        #self.wait_for('EXITING CONSOLE MODE')
    
    def archive(self):
        self.archive_intro()
        data = self.get_data_until('ARCHIVED')
        self.outro()
        return data        
        
    def download_sample(self):
        self.console_mode()
        self.main_menu()
        self.file_menu()
        self.send('1')
        self.wait_for('RECEIVE NOW')
        data = self.get_data_until('TRANSFER DONE')
        print("GOT DATA:{}".format(data))
        self.outro()
        return data
    
    def download_log(self):#, download_type):
        self.console_mode()
        self.main_menu()
        self.file_menu()
        self.send('4')
        self.wait_for('Upload Log')
        self.send('7')
        self.wait_for('UPLOADING LOG')
        data = self.get_data_until('UPLOAD DONE')
        #self.outro()
        return data
    
    def truncate_log(self):
        self.truncate_log()
        self.outro()
        return data

    def upload(self, json_object):
        self.console_mode()
        self.file_menu()
        self.send('3')
        self.wait_for('BEGIN_SEND')
        self.upload_json(json_object)
        self.wait_until('TRANSFER DONE')
        self.outro()
        
    def console_mode(self):
        # Keep trying until success or timeout.
        self.send('~2')
        self.wait_for('ENTERING CONSOLE MODE')
    def main_menu(self):
        self.send('u')
        self.wait_for('Sampling')
    def file_menu(self):
        self.send('3')
        self.wait_for('File Ops')
    
     
if __name__ == '__main__':
    # Some example usage
    for possible_port in ArduinoMega2560.list_ports():
        print (possible_port)

    with ArduinoMega2560() as arduino:
        print (arduino.port)

