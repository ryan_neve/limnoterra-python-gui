from tkinter import *
import json
import tkinter
import tkinter.filedialog as filedialog
import gui_layout
from arduino_mega import ArduinoMega2560
#from logo import planktos_logo


class Planktos_GUI(Frame):
    # Included here instead of in gui_constants so they can be bound to
    # internal methods
                    
                         
    def __init__(self, parent):
        '''Creates the Planktos Instruments GUI'''
        Frame.__init__(self, parent)
        self.parent = parent
        self.ports = {desc:com for com, desc, _ in ArduinoMega2560.list_ports()}
        self.transfer_text = ''
        self.download_file_name = ''
        self.download_file_size = 0
        self.pack(fill=BOTH, expand=YES)
        self.UPLOAD_OPTIONS = [('Users', 'USER_ID.csv'),
                               ('Species', 'SPECIES.csv'),
                               ('Vessels', 'VESSELS.csv'),
                               ('Quick Species', 'QSPEC.csv'),
                               ('Areas', 'AREAS.csv')]

                          
        self.DOWNLOAD_OPTIONS = ['Sampling Data',
                                 'Log File']
        
        self.ACTION_LIST_OPTIONS = ['Download Data From Board',
                                    'Download Log From Board',
                                    'Upload Configuration to Board']
                                    
        self.AVAILABLE_BAUDRATES = [9600, 19200, 28800, 38400, 57600, 115200]
        
        # We need to close the serial port when the window closes
        self.parent.protocol("WM_DELETE_WINDOW", self.on_closing)
        self.get_config_variables()
        self.create_state_variables()
        self.initUI()
    
    def on_closing(self):
        ''' closes serial port'''
        self.arduino.close()
        self.parent.destroy()

    def initUI(self):
        '''How the GUI elements are created'''
        self.create_texts()
        self.create_widgets()

    def create_texts(self):
        '''Creates all plain text elements'''
        font = (16,)
        for text, position in gui_layout.TEXT_POSITIONS.items():
            x, y = position
            # We need to change this one as self.operation_choice changes
            if 'Transfer' in text:
                self.transfer_lbl = Label(self, 
                                          textvariable=self.transfer, 
                                          font=(12,), justify=LEFT)
                self.transfer_lbl.place(x=x, y=y, anchor=W)
                continue
            lbl = Label(self, text=text, font=font)
            lbl.place(x=x, y=y, anchor=W)

    def create_state_variables(self):
        '''Creates variables for storing widget data'''
        self.action_choice = StringVar(self)
        self.operation_choice = StringVar(self)
        self.operation_list = self.DOWNLOAD_OPTIONS
        self.port_choice = StringVar(self)
        self.raw_checked = BooleanVar(self)
        self.transfer = StringVar(self)
        self.preview = ''
        self.preview_show = StringVar(self)
        self.preview_show.set('')
        self.send_data = StringVar(self)
        
        self.action_choice.set(self.ACTION_LIST_OPTIONS[0])
        self.operation_choice.set(self.DOWNLOAD_OPTIONS[0])
        self.port_choice.set(list(self.ports.keys())[0])
        self.raw_checked.set(False)
        
        baudrate = int(self.baudrate.get())
        timeout = int(self.timeoutstring.get())
        port = self.ports[self.port_choice.get()]
        
        self.arduino = ArduinoMega2560(port=port, 
                                       timeout=timeout,
                                       baudrate=baudrate)

    def config(self):
        '''Creates new window for serial configuration'''
        # Config Data
        self.old_baudrate = self.baudrate.get()
        self.old_download_location = self.download_location
        self.old_upload_location = self.upload_location
        self.old_timeout = self.timeoutstring.get()
        
        # Actual Window Parameters
        self.config_window = Toplevel(self)
        self.config_window.geometry('450x250+400+400')
        self.config_window.resizable(0, 0)
        self.config_window.title('Planktos Instruments File Transfer Utility')
        self.config_window.protocol("WM_DELETE_WINDOW", self.config_cancel)
        
        # Window Elements
        self.create_config_texts()
        self.create_config_widgets()

    def create_widgets(self):
        '''Creates all buttons, checkboxes, lists, and text boxes'''
        # Action
        action_menu = OptionMenu(self, self.action_choice,
                                 *[x for x in self.ACTION_LIST_OPTIONS],
                                 command=self.update_action_button)
        x, y = gui_layout.WIDGET_POSITIONS['Action']
        action_menu.place(x=x, y=y, w=210, anchor=W)

        # Operation.  Save because we need to configure this button later
        self.op_menu = OptionMenu(self, self.operation_choice,
                                         *self.operation_list)
        x, y = gui_layout.WIDGET_POSITIONS['Operation']
        self.op_menu.place(x=x, y=y, w=120, anchor=W)

        # Config
        config_button = Button(self, text='CONFIG',
                               command=self.config,
                               background='lightblue')
        x, y = gui_layout.WIDGET_POSITIONS['Config']
        config_button.place(x=x, y=y, w=70, anchor=W)

        # Port
        port_menu = OptionMenu(self, self.port_choice, *self.ports.keys())
        x, y = gui_layout.WIDGET_POSITIONS['Port']
        port_menu.place(x=x, y=y, w=210, anchor=W)
        
        # Data
        data_field = Label(self, textvariable=self.preview_show, 
                           relief=GROOVE, justify=LEFT, anchor=NW)
        x, y = gui_layout.WIDGET_POSITIONS['Data']
        data_field.place(x=x, y=y, w=750, h=340, anchor=NW)
        
        # Transfer
        transfer_button = Button(self, text='Begin Transfer',
                                 command=self.do_transfer,
                                 background='lightgreen', font=(16,))
        x, y = gui_layout.WIDGET_POSITIONS['Transfer']
        transfer_button.place(x=x, y=y, anchor=W)
        
        # Archive.  Save because we need to configure this button later
        self.archive_button = Button(self, text='Archive Data',
                                     command=self.archive_clicked,
                                     background='lightyellow', font=(16,))
        x, y = gui_layout.WIDGET_POSITIONS['Archive']
        self.archive_button.place(x=x, y=y, anchor=W)
        
        # Raw
        raw_data = Checkbutton(self, variable=self.raw_checked, 
                               text='Raw', font=(12,))
        x, y = gui_layout.WIDGET_POSITIONS['Raw']
        raw_data.place(x=x, y=y, anchor=W)
        
        # Copy to clipboard button
        raw_data = Button(self, command=self.copy_to_clipboard, 
                          text='Copy', font=(12,))
        x, y = gui_layout.WIDGET_POSITIONS['Copy']
        raw_data.place(x=x, y=y, anchor=W)
        
        # Send data Entry
        send_entry = Entry(self, textvariable=self.send_data)
        x, y = gui_layout.WIDGET_POSITIONS['Send Data']
        send_entry.place(x=x, y=y, w=680, h=30, anchor=W)
        
        # Send
        send_button = Button(self, command=self.send_clicked, 
                          text='Send', font=(12,))
        x, y = gui_layout.WIDGET_POSITIONS['Send']
        send_button.place(x=x, y=y, anchor=W)
        
        # Set initial state of Upload file menu
        self.update_action_button()
        
    def copy_to_clipboard(self):
        '''Clears clipboard and copies data in preview area to it'''
        self.parent.clipboard_clear()
        self.parent.clipboard_append(self.preview)
    
    def send_clicked(self):
        '''Sends data directly to arduino'''
        with ArduinoMega2560(port=self.port_choice.get(), 
                             timeout=int(self.timeoutstring.get()),
                             baudrate=self.baudrate.get()) as arduino:
            for line in self.send_data.get().split():
                arduino.send(line)        
        
    def update_action_button(self, *args):
        action = self.action_choice.get()
        
        # Set selection and menu
        if action == 'Download Data From Board':
            self.operation_list = self.DOWNLOAD_OPTIONS
            self.archive_button.config(text='Archive Data', state=NORMAL)
            self.transfer.set('Download To:\n{}'.format(self.download_location))
        elif action == 'Upload Configuration to Board':
            self.operation_list = [x[0] for x in self.UPLOAD_OPTIONS]
            self.transfer.set('Upload From:\n{}'.format(self.upload_location))
            self.archive_button.config(state=DISABLED)
        else:
            self.operation_list = ['']
            self.archive_button.config(text='Truncate Log', state=NORMAL)
            self.transfer.set('Transfer To:\n{}'.format(self.download_location))
            
        
        # Update menu
        self.op_menu['menu'].delete(0, 'end')
        for op in self.operation_list:
            self.op_menu['menu'].add_command(label=op, 
                                             command=tkinter._setit(self.operation_choice, op))
                                             
        if self.operation_choice.get() not in self.operation_list:
            self.operation_choice.set(self.operation_list[0])
    
    def create_config_texts(self):
        '''Creates all plain text elements'''
        font = (12,)
        for text, position in gui_layout.CONFIG_TEXT_POSITIONS.items():
            x, y = position
            lbl = Label(self.config_window, text=text,  font=font)
            lbl.place(x=x, y=y, anchor=E)
    
    def create_config_widgets(self):
        '''Creates config window widgets'''
        # Download Button
        download = Button(self.config_window, text='Select', 
                          command=self.config_download_clicked)
        x, y = gui_layout.CONFIG_WIDGET_POSITIONS['Download']
        download.place(x=x, y=y, anchor=W)
        
        # Upload Button
        upload = Button(self.config_window, text='Select', 
                        command=self.config_upload_clicked)
        x, y = gui_layout.CONFIG_WIDGET_POSITIONS['Upload']
        upload.place(x=x, y=y, anchor=W)
        
        # Baudrate Menu
        baud = OptionMenu(self.config_window, self.baudrate, 
                          *self.AVAILABLE_BAUDRATES)
        x, y = gui_layout.CONFIG_WIDGET_POSITIONS['Baud']
        baud.place(x=x, y=y, w=80, anchor=W)
        
        # Timeout
        timeout = Entry(self.config_window, textvariable=self.timeoutstring)
        x, y = gui_layout.CONFIG_WIDGET_POSITIONS['Timeout']
        timeout.place(x=x, y=y, w=50, anchor=W)
        
        # Ok Button
        ok = Button(self.config_window, text='Ok', font=(20,), 
                    command=self.save_info)
        x, y = gui_layout.CONFIG_WIDGET_POSITIONS['Ok']
        ok.place(x=x, y=y, h=60, w=100, anchor=W)
        
        # Cancel Button
        cancel = Button(self.config_window, text='Cancel', font=(20,), 
                        command=self.config_cancel)
        x, y = gui_layout.CONFIG_WIDGET_POSITIONS['Cancel']
        cancel.place(x=x, y=y, h=60, w=100, anchor=W)
        
    def get_config_variables(self):
        '''Sets state variables'''
        self.baudrate = IntVar(self)
        self.timeoutstring = StringVar(self)
        self.download_location = "C:\\temp"
        self.upload_location = "C:\\temp"
        
        # These can be taken from a config file in the future
        self.baudrate.set(57600)
        self.timeoutstring.set(5)
    
    # CONFIG WINDOW FUNCTIONS
    def config_download_clicked(self):
        '''Once download button is clicked...'''
        self.download_location = filedialog.askdirectory(parent=self.config_window, title="Select directory to download data to")
        self.update_action_button()
    
    def config_upload_clicked(self):
        '''Once upload button is clicked...'''
        self.upload_location = filedialog.askdirectory(parent=self.config_window, title="Select directory containing configuration ")
        self.update_action_button()
        
    def save_data(self, data):
        '''saves data to self.download_location'''
        # Currently this automatically overwrites whatever is there.  Beware.
        file_with_path = self.download_location + "\\" + self.download_file_name
        print("OPENING FILE:{}".format(file_with_path))
        with open(file_with_path, 'wb') as write_file:
            write_file.write(bytes(data,'UTF-8'))
        
    def archive_clicked(self):
        '''Controls which functions are called once archive button is pressed'''
        action = self.action_choice.get()
        if action == 'Download Data From Board':
            self.archive_data()
        elif action == 'Download Log From Board':
            self.log()
        
    # MAIN FUNCTIONS    
    def log(self):
        '''Downloads log from board'''        
        if self.download_location is None:
            return
        data = self.arduino.download_log()
        if self.raw_checked.get():
            self.preview = data
        else:
            self.download_file_name, self.download_file_size, self.preview = self.get_file_metadata(data)
        self.preview_show.set(self.preview)
        self.save_data(data)
    
    def archive_data(self):
        '''Archives data once button is pressed'''
        if self.download_location is None:
            return
        data = self.arduino.archive()
        if self.raw_checked.get():
            self.preview = data
        else:
            self.download_file_name, self.download_file_size, self.preview = self.get_file_metadata(data)
        self.preview_show.set(self.preview)
        self.save_data(data)
            
    def save_info(self):
        '''saves information into class'''        
        baudrate = int(self.baudrate.get())
        timeout = int(self.timeoutstring.get())
        port = self.ports[self.port_choice.get()]
        
        self.arduino = ArduinoMega2560(port=port, 
                                       timeout=timeout,
                                       baudrate=baudrate)
        self.config_window.destroy()
    
    def get_file_metadata(self, data):
        '''Parses and returns file metadata: FILENAME, SIZE, DATA'''
        # Data follows this convention
        #{FILENAME:FB_LOG.CSV,SIZE:<size in bytes>,DATA:<log file contents>}\r
        # make json & windows friendly
        data.replace('\n','\\n')
        data.replace('\r','\\r')
        data = data[data.find("{"):data.rfind("}") + 1] # Just what's between the outermost {}
        print("JSON INPUT:{}".format(data))
        data = json.loads(data,strict=False)
        print("JSON OUTPUT:{}".format(data))
        return data['FILENAME'], int(data['SIZE']), data['DATA']
        '''
        data = data.strip().replace('{', '').replace('}', '')
        data = {k:v for entry in data.split(',') for k, v in entry.split(':')}
        return data['FILENAME'], int(data['SIZE']), data['DATA']        
        '''
    
    def config_cancel(self):
        '''actions performed when config button is explicitly cancelled'''
        self.baudrate.set(self.old_baudrate)
        self.timeoutstring.set(self.old_timeout)
        self.download_location = self.old_download_location
        self.upload_location = self.old_upload_location
        self.config_window.destroy()
    
    def do_transfer(self):
        '''Transfer data to/from the arduino based on GUI input'''
        print("transfer()")
        action = self.action_choice.get()
        print("Action:{}".format(action))
        if action == 'Download Data From Board':
            # Can't do much if data locations haven't yet been set...
            if self.download_location is None:
                # Need error message.
                println("No download location set");
                return
            raw_data = self.arduino.download_sample()
            print("DATA:{}".format(raw_data))
            self.download_file_name, self.download_file_size, self.preview = self.get_file_metadata(raw_data)
            if self.raw_checked.get():
                self.preview_show.set(raw_data)
            else:
                self.preview_show.set(self.preview)
            self.save_data(self.preview)
            
        elif action == 'Download Log From Board':
            # Can't do much if data locations haven't yet been set...
            if self.download_location is None:
                return
            raw_data = self.arduino.download_log()
            if self.raw_checked.get():
                self.preview_show.set(raw_data)
            else:
                self.preview_show.set(self.preview)
            self.save_data(self.preview)
            
        elif action == 'Upload Configuration to Board':
            # Can't do much if data locations haven't yet been set...
            if self.upload_location is None:
                return
            for name, f in self.UPLOAD_OPTIONS():
                if name == self.operation_choice.get():
                    file = f
                    break
            with open(file, 'rb') as data_file:
                data = data_file.readlines()
            download_file_name, self.download_file_size, data_to_upload = self.get_file_metadata(data)
            self.arduino.upload(data_to_upload)
            self.preview = data
            self.preview_show.set(self.preview)
        
        
class Splash(Frame):
    VERSION = '1.0'
    
    
    def __init__(self, parent):
        '''Creates the Planktos Instruments GUI'''
        Frame.__init__(self, parent)
        self.parent = parent
        self.pack(fill=BOTH, expand=YES)
        #pillow.Image()
        LOGO = PhotoImage(data=planktos_logo)
        self.logo = planktos_logo
        logo = Label(self, image=LOGO)
        logo.pack()

if __name__ == '__main__':
    root = Tk()
    root.geometry('850x550+300+300')
    root.resizable(0,0)
    root.title('Planktos Instruments File Transfer Utility')
    #Splash(root)
    Planktos_GUI(root)
    root.mainloop()
